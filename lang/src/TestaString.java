public class TestaString {

    public static void main(String[] args) {
        String nome = "Socorram-me, subi no ônibus em Marrocos";
        TestaString.inverteComStringBuilder(nome);

        System.out.println(args[0]);
    }

    private static void inverteComStringBuilder(String texto) {
        System.out.println("\t");
        StringBuilder invertido = new StringBuilder(texto).reverse();
        System.out.println(invertido);
    }
}

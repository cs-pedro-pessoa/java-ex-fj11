import java.math.BigDecimal;
import java.util.Formatter;

/**
 * Created by pedro on 29/08/16.
 */
public abstract class Conta implements Comparable<Conta>{

    protected int numero;
    protected double saldo;
    protected String nome;



    public Conta(int numero, String nome) {
        this.numero = numero;
        this.nome = nome;
    }


    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void deposita(double valor){
        this.saldo += valor;
    }

    public int compareTo(Conta outraConta){
        return this.getNumero() - outraConta.getNumero();
    }


    @Override
    public String toString() {
        return "Número: " + getNumero() + " " + "Saldo: " + getSaldo() + " " + "Nome: " + getNome();
    }

    public abstract int compareTo(ContaPoupanca outraConta);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Conta conta = (Conta) o;

        return numero == conta.numero;

    }

    @Override
    public int hashCode() {
        return numero;
    }
}

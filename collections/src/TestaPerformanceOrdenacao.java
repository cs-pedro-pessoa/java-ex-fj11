import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by pedro on 29/08/16.
 */
public class TestaPerformanceOrdenacao {

    public static void main(String[] args) {
        System.out.println("Inciando... ");
        long inicio = System.currentTimeMillis();

        //List<Integer> teste = new ArrayList<>();
        List<Integer> teste = new LinkedList<>();

        for (int i = 0; i < 30000; i++) {
            teste.add(0, i);
        }

        long fim = System.currentTimeMillis();
        double tempo = (fim - inicio) / 1000.0;

        System.out.println("Tempo gasto: " + tempo);
    }
}

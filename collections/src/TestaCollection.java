import java.util.*;

/**
 * Created by pedro on 29/08/16.
 */
public class TestaCollection {

    public static void main(String[] args) {
        //List<ContaPoupanca> contas = new ArrayList<ContaPoupanca>();

        //List<ContaPoupanca> contas = new LinkedList<ContaPoupanca>();

        List<Conta> contas = new ArrayList<Conta>();

        Random random = new Random();

        //ContaPoupanca cp1 = new ContaPoupanca(900, "Pedro");
        ContaPoupanca cp1 = new ContaPoupanca(random.nextInt(2000), "Pedro");
        //cp1.deposita(1000);

        cp1.deposita(random.nextInt(1000) + random.nextDouble());
        contas.add(cp1);

        //ContaPoupanca cp2 = new ContaPoupanca(500, "Paulo");
        ContaPoupanca cp2 = new ContaPoupanca(random.nextInt(2000), "Paulo");
        //cp2.deposita(2500);

        cp2.deposita(random.nextInt(1000) + random.nextDouble());
        contas.add(cp2);

        //ContaPoupanca cp3 = new ContaPoupanca(50, "Cesar");
        ContaPoupanca cp3 = new ContaPoupanca(random.nextInt(2000), "Cesar");
        //cp3.deposita(4000);

        cp3.deposita(random.nextInt(1000) + random.nextDouble());
        contas.add(cp3);



        Collections.sort(contas);
        //Collections.shuffle(contas);
        //Collections.rotate(contas, 10);

        System.out.println(contas.size());

        for (Conta conta : contas){
            //System.out.println("Número: " + conta.getNumero() + " " + "Saldo: " + conta.getSaldo());
            System.out.println(conta.toString());
        }






    }
}

import java.util.HashSet;

/**
 * Created by pedro on 29/08/16.
 */
public class TestaHashSetDeConta {

    public static void main(String[] args) {
        HashSet<Conta> contas = new HashSet<Conta>();

        ContaCorrente c1 = new ContaCorrente(123, "Gabriel");
        ContaCorrente c2 = new ContaCorrente(123, "Laura");
        ContaCorrente c3 = new ContaCorrente(404, "Not Found");

        contas.add(c1);
        contas.add(c2);
        contas.add(c3);

        System.out.println("Total de contas dentro do HashSet: " + contas.size());
    }
}

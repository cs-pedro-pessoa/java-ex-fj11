/**
 * Created by pedro on 29/08/16.
 */
public class ContaPoupanca extends Conta /* implements Comparable<ContaPoupanca> */{


    public ContaPoupanca(int numero, String nome){
        super(numero, nome);
    }
    @Override
    public int compareTo(ContaPoupanca outraConta) {
        //return this.getNumero() - outraConta.getNumero();
        return this.getNome().compareTo(outraConta.getNome());
    }
}

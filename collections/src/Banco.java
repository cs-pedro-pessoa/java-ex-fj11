import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pedro on 29/08/16.
 */
public class Banco {

    private List<Conta> contas = new ArrayList<Conta>();
    private Map<String, Conta> indexadoPorNome = new HashMap<String, Conta>();

    public void adicionaContas(Conta c) {
        contas.add(c);
    }

    public Conta pegaConta(int posicao) {
        return contas.get(posicao);
    }

    public int pegaQuantidadeDeContas() {
        return contas.size();
    }

    public Conta buscaPorNome(String nome) {
        return indexadoPorNome.get(nome);
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }

    public Map<String, Conta> getIndexadoPorNome() {
        return indexadoPorNome;
    }

    public void setIndexadoPorNome(Map<String, Conta> indexadoPorNome) {
        this.indexadoPorNome = indexadoPorNome;
    }
}

/**
 * Created by pedro on 29/08/16.
 */
public class ContaCorrente extends Conta {
    public ContaCorrente(int numero, String nome) {
        super(numero, nome);
    }

    @Override
    public int compareTo(ContaPoupanca outraConta) {
        return getNome().compareTo(outraConta.getNome());
    }
}

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pedro on 29/08/16.
 */
public class TestaMap {

    public static void main(String[] args) {
        Conta c1 = new ContaCorrente(500, "Pedro");
        c1.deposita(10000);

        Conta c2 = new ContaCorrente(600, "Lucas");
        c2.deposita(3000);

        Map<String, Conta>mapaDeContas = new HashMap<String, Conta>();

        mapaDeContas.put("diretor", c1);
        mapaDeContas.put("gerente", c2);

        Conta contaDoDiretor =  mapaDeContas.get("diretor");
        System.out.println(contaDoDiretor.getSaldo());
    }
}

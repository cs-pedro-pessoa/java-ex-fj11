import java.util.*;

/**
 * Created by pedro on 29/08/16.
 */
public class TestaTreeSet {

    public static void main(String[] args) {
        //TreeSet<Integer> teste = new TreeSet<Integer>();
        List<Integer> teste = new ArrayList<Integer>();

        for (int i = 0; i < 1000; i++) {
            teste.add(i);
        }

        /* for(Integer i : teste.descendingSet()){
            System.out.println(i + " ");
        } */

        Collections.reverse(teste);

        for (Integer i : teste) {
            System.out.println(i + " ");
        }
    }
}

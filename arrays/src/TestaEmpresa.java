/**
 * Created by pedro on 25/08/16.
 */
public class TestaEmpresa {
    public static void main (String[] args){

        Empresa empresa = new Empresa();

        empresa.funcionario = new Funcionario[10];

        Funcionario f1 = new Funcionario();
        f1.salario = 1500;
        f1.dataEntrada = new Data(7, 1, 2016);
        empresa.adiciona(f1);

        Funcionario f2 = new Funcionario();
        f2.salario = 2000;
        f2.dataEntrada = new Data(5, 10, 2016);
        empresa.adiciona(f2);

        Funcionario f3 = new Funcionario();
        f3.salario = 2500;
        f3.dataEntrada = new Data(4, 3, 2015);
        empresa.adiciona(f3);

        //empresa.funcionario[0].mostra();
        //empresa.funcionario[1].mostra();
        //empresa.funcionario[2].mostra();

        //empresa.mostraFuncionarios();

        empresa.mostraTodasAsInformacoes();
        empresa.contemFuncionario(f1);


    }
}

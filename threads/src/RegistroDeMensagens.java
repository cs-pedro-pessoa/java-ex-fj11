import java.util.*;

/**
 * Created by pedro on 29/08/16.
 */
public class RegistroDeMensagens {

    public static void main(String[] args) throws InterruptedException{
        //Collection<String> mensagens = new Vector<String>();
        Collection<String> mensagens = new HashSet<String>();
        //Collection<String> mensagens = new LinkedList<String>();

        Thread t1 = new Thread(new ProduzMensagens(0, 10000, mensagens));
        Thread t2 = new Thread(new ProduzMensagens(10000, 20000, mensagens));
        Thread t3 = new Thread(new ProduzMensagens(20000, 30000, mensagens));

        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t2.join();

        System.out.println("Threads finalizadas!");

        for (int i = 0; i < 15000; i++) {
            if (!mensagens.contains("Mensagem " + i)) {
                throw new IllegalStateException("mensagem não encontrada: " + i);
            }
        }

        if (mensagens.contains(null)) {
            throw new IllegalStateException("mensagem não pode ser nula!");
        }

        System.out.println("Fim da execucao com sucesso");
    }
}

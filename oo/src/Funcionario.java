/**
 * Created by pedro on 24/08/16.
 */
public class Funcionario {

    String nome;
    String departamento;
    double salario;
    Data dataEntrada;
    String rg;
    boolean estaNaEmpresa;


    void recebeAumento(double aumento){
        this.salario = aumento;
    }

    void demite(){

    }

    double calculaGanhoAtual(){
        return salario * 12;
    }



    void mostra(){
        System.out.println("nome: " + nome);
        //System.out.println("departamento: " + departamento);
        System.out.println("data de entrada: " + this.dataEntrada.getFormatada());
        //System.out.println("rg: " + rg);
        //System.out.println("esta na empresa?: " + estaNaEmpresa);
        System.out.println("salario atual: " + salario);
        System.out.println("ganho anual: " + calculaGanhoAtual());
    }
}

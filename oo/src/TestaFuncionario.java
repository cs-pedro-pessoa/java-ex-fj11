/**
 * Created by pedro on 24/08/16.
 */
public class TestaFuncionario {

    public static void main(String[] args){
        Funcionario f1 = new Funcionario();
        f1.nome = "Pedro";
        f1.salario = 100;
        f1.recebeAumento(200);

        f1.dataEntrada = new Data(1, 7, 2009);

        //Funcionario f2 = new Funcionario();
        //f2.nome = "Pedro";
        //f2.salario = 100;

        Funcionario f2 = f1;

        if(f1 == f2){
            System.out.println("iguais");
        } else{
            System.out.println("diferentes");
        }

        f1.mostra();
        System.out.println(f2.nome);
    }
}

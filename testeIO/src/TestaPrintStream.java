import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Created by pedro on 29/08/16.
 */
public class TestaPrintStream {

    public static void main(String[] args) throws IOException{

        Scanner entrada = new Scanner(System.in);
        PrintStream saida = new PrintStream("saida2.txt");

        System.out.println("Mensagem: ");

        while(entrada.hasNext()){
            saida.println(entrada.nextLine());
        }

        saida.close();
        entrada.close();


    }
}

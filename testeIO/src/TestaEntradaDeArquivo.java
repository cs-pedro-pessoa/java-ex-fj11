import java.io.*;
import java.util.Scanner;

/**
 * Created by pedro on 29/08/16.
 */
public class TestaEntradaDeArquivo {

    public static void main(String[] args) throws IOException{
        InputStream is = new FileInputStream("entrada.txt");
        Scanner sc = new Scanner(is);

        OutputStream os = new FileOutputStream("saida.txt");
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        //System.out.println("Mensagem a ser digitada: ");

        while(sc.hasNextLine()){
            //System.out.println(sc.nextLine());
            String linha = sc.nextLine();
            bw.write(linha);
            bw.newLine();
        }

        sc.close();

    }
}

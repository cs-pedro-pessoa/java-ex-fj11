import java.io.*;

/**
 * Created by pedro on 29/08/16.
 */
public class TestaIO {

    public static void main(String[] args) throws IOException{
        //InputStream is = System.in;

        //InputStream is = new FileInputStream("entrada.txt");
        //InputStreamReader isr = new InputStreamReader(is);
        //BufferedReader br = new BufferedReader(isr);

        BufferedReader br = new BufferedReader(
                                new InputStreamReader(
                                    new FileInputStream("entrada.txt")));



        System.out.println("Mensagem: ");
        String s = br.readLine();

        while (s != null){
            System.out.print(s);
            s = br.readLine();
        }

        br.close();
    }
}
